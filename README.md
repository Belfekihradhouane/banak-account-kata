
Bank account kata:


![build](https://travis-ci.com/Belfekihradhouane/banak-account-kata.svg?branch=master)
[![test coverage](https://codecov.io/gh/Belfekihradhouane/banak-account-kata/branch/master/graph/badge.svg)](https://codecov.io/gh/<your-name>/<project-name>)


Think of your personal bank account experience When in doubt, go for the simplest solution

Requirements:

Deposit and Withdrawal 
Account statement (date, amount, balance)
Statement printing

User Stories:

1 First user story:

To save money i need to diposit it in my account

2 Second user story:

To retive money i need to withdraw money from my account

3 therd user story:

To verify all the operation in my account I need to show the history (transaction, date, amount, balance) of each operation
For more information:
