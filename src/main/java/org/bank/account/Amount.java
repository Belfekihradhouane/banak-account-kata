package org.bank.account;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

@Value
@EqualsAndHashCode
public class Amount {

    BigDecimal value;

    public Amount(BigDecimal value) {

        this.value = value;
    }

    public Amount plus(Amount amount) {
        final var value = this.value.add(amount.value);
        return new Amount(value);
    }

    public boolean isNegative() {
        return this.value.compareTo(ZERO) < 0;
    }

    public static Amount of(double value) {
        return new Amount(new BigDecimal(value));
    }

    public Amount negate() {
        final var value = this.value.negate();
        return new Amount(value);
    }

}
